namespace PrisonerDilemma;

/// <summary>
/// The prisoner's dilemma, repeated a fixed number of times. Allowing prisoners
/// in the situation to learn the other's behavior. See:
/// https://en.wikipedia.org/wiki/Prisoner's_dilemma#The_iterated_prisoner's_dilemma
/// </summary>
class IteratedDilemma
{
  /// <summary>
  /// Parameters of the generalized form of the dilemma (the score for each
  /// reward).
  /// </summary>
  public DilemmaParameters Parameters { get; }

  private int NumIterations { get; }

  public IteratedDilemma(int numIterations)
  {
    // TODO : What kind of initializing syntax is used here?
    Parameters = new DilemmaParameters
    {
      CooperateReward = 0.5,
      SnitchReward = 0.75,
      CaughtReward = 0,
      DefectReward = 0.25
    };
    NumIterations = numIterations;
  }

  // TODO : What is that return data type?
  private (double firstPrisonerReward, double secondPrisonerReward)
  ResolveDilemma(
    PrisonerChoice firstPrisonerChoice, PrisonerChoice secondPrisonerChoice
  )
  {
    if (firstPrisonerChoice == secondPrisonerChoice)
    {
      if (firstPrisonerChoice == PrisonerChoice.Cooperate)
      {
        // Both prisoners cooperated
        return (Parameters.CooperateReward,
                Parameters.CooperateReward);
      }
      else
      {
        // Both prisoners defected
        return (Parameters.DefectReward, Parameters.DefectReward);
      }
    }
    else
    {
      if (firstPrisonerChoice == PrisonerChoice.Cooperate)
      {
        // First prisoner cooperated, but not the second one
        return (Parameters.CaughtReward, Parameters.SnitchReward);
      }
      else
      {
        // Second prisoner cooperated, but not the first one
        return (Parameters.SnitchReward, Parameters.CaughtReward);
      }
    }
  }

  /// <summary>
  /// Simulates the prisoner's dilemma a number of time (given to the
  /// constructor) and returns the cumulative score of each prisoner (averaged
  /// over the number of iterations).
  /// </summary>
  public (double firstPrisonerScore, double secondPrisonerScore)
  Simulate(IPrisoner firstPrisoner, IPrisoner secondPrisoner)
  {
    double firstPrisonerScore = 0, secondPrisonerScore = 0;

    for (var i = 0; i < NumIterations; i++)
    {
      // Both prisoners make their choice blindly ...
      PrisonerChoice firstPrisonerChoice = firstPrisoner.GetChoice();
      PrisonerChoice secondPrisonerChoice = secondPrisoner.GetChoice();
      // ... They then get told the result
      firstPrisoner.ReceiveOtherChoice(secondPrisonerChoice);
      secondPrisoner.ReceiveOtherChoice(firstPrisonerChoice);

      // TODO : What kind of assignment syntax is that?
      (double firstPrisonerReward, double secondPrisonerReward) =
        ResolveDilemma(
          firstPrisonerChoice, secondPrisonerChoice
        );

      firstPrisonerScore += firstPrisonerReward;
      secondPrisonerScore += secondPrisonerReward;
    }

    // Normalizing scores with the number of iterations, so that iterated
    // dilemmas with different number of iterations are still comparable.
    return (firstPrisonerScore / NumIterations,
      secondPrisonerScore / NumIterations);
  }
}