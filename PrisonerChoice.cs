namespace PrisonerDilemma;

// TODO : Is an enum a reference or a value type?
/// <summary>
/// In the prisoner's dilemma, each prisoner can either cooperate or defect.
/// </summary>
enum PrisonerChoice
{
  /// <summary>
  /// Prisoner chooses to cooperate with the other prisoner (i.e. they remain
  /// silent).
  /// </summary>
  Cooperate,
  /// <summary>
  /// Prisoner chooses to testify against the other prisoner.
  /// </summary>
  Defect
}